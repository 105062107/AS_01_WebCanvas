基本功能:
#BASIC CONTROL TOOLS
1.Brush and Eraser
    可以選是要刷子還是橡皮擦，選擇其中一個另一個會被取消
2.color selector
    可以直接選顏色，所有的東西包刮畫長方形等都會是那個顏色
3.Simple menu
    可以直接選擇刷子大小，又甚至是橡皮擦大小
Button1_onclick()
#TEXT INPUT
1.User can type texts on canvas 
2.Font menu (typeface and size) 
    先選擇字體大小，然後再按輸入，接著去點畫面上自己想要的位子，就會出現可以打字的框框，打好之後按下enter鍵就可以直接輸入
    在輸入旁邊有下拉式選單可以選擇字體大小的功能，這裡將字形設定成5-14
#CURSOR ICON
1.The image should change according to the currently used tool 
    會根據不同的狀況，例如說是用橡皮擦，或者是用一般刷筆，又或者是拉長方形之類的，游標都會變化
#REFRESH BUTTON
1.Reset canvas
    直接按下重做鍵即可回到最原本的狀態

進階功能:
#DIFFERENT BRUSH SHAPE
1.Circle, rectangle and triangle 
    下面有筆刷按紐，有一般的還有圓形和正方形的，建議開成10PX的筆粗會比較明顯
    或是使用殘影功能還有調整筆刷粗細功能，越大，筆刷樣式更明顯
    (記得開啟筆刷大小才能使用這個功能)
#UN/RE-DO BUTTON
    可以按下重做或者是復原鍵，連畫長方形圓形三角形之類的也可，殘影功能也可以復原
#IMAGE TOOL
    可以上傳照片，點上傳按紐
#DOWNLOAD
    可以下載照片，點下載按紐

Other useful widgets:
#畫長方形，畫圓形
    點了之後直接在小畫家上畫即可
#畫三角形
    畫三角形的方法比較特別，首先要用滑鼠拖曳，放開之後連成第一條線，在去點你要的第二個頂點的位子，如此一來會直接確認出一個三角形
#方形，圓形，三角形殘影功能
    直接點選殘影功能的按鈕在，接著再到小畫家面板上塗鴉，就會出現有殘影的圖示，畫慢一點可以當特殊筆刷用，例如三角形筆刷


